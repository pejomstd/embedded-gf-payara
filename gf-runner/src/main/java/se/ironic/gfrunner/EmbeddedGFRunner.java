package se.ironic.gfrunner;

import java.io.File;
import org.glassfish.embeddable.BootstrapProperties;
import org.glassfish.embeddable.GlassFish;
import org.glassfish.embeddable.GlassFishException;
import org.glassfish.embeddable.GlassFishProperties;
import org.glassfish.embeddable.GlassFishRuntime;

public class EmbeddedGFRunner {

    public static void main(String[] args) throws GlassFishException {
        if(args.length != 3) {
            System.out.println("Usage > EmbeddedGFRunner \"port\" \"deployable.war\" \"contextRoot\"");
            System.exit(1);
        }
        int port = Integer.parseInt(args[0]);
        BootstrapProperties bootstrap = new BootstrapProperties();
        GlassFishRuntime runtime = GlassFishRuntime.bootstrap();
        GlassFishProperties glassfishProperties = new GlassFishProperties();
        glassfishProperties.setPort("http-listener", port);
        GlassFish glassfish = runtime.newGlassFish(glassfishProperties);
        glassfish.start();
        glassfish.getDeployer().deploy(new File(args[1]), "--contextroot", args[2]);
    }
}
