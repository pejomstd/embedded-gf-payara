package se.ironic.task.boundary;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import se.ironic.task.model.Task;

/**
 *
 * @author Ironic
 */
@Path("tasks")
public class TaskResource {
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Task> getAll() {
        return TaskCache.INST.getTasks();
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response create(Task task) {
        TaskCache.INST.putTask(task);
        URI id = URI.create("tasks/" + task.getId());
        Response resp = Response.created(id).build();
        return resp;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{id}")
    public Response getTask(@PathParam("id") String id) {
        Optional<Task> task = TaskCache.INST.getTask(id);
        Response resp = null;
        if (task.isPresent()) {
            resp = Response.ok(task.get()).build();
        } else {
            resp = Response.noContent().build();
        }
        return resp;
    }
    
    @PUT @Consumes(MediaType.APPLICATION_JSON)
    @Path("{id}")
    public Response update(Task task) {
        // Just replace the one there
        TaskCache.INST.putTask(task);
        return Response.ok().build();
    }
    
    @DELETE
    @Path("{id}")
    public Response delete(@PathParam("id") String id) {
        System.out.println("Deleting task: " + id);
        TaskCache.INST.removeTask(id);
        return Response.ok().build();
    }
}
