package se.ironic.task.boundary;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import se.ironic.task.model.Task;

public enum TaskCache {
    INST;
    
    private Map<String, Task> tasks;

    private TaskCache() {
        tasks = new HashMap<>();
    }
    
    public Optional<Task> getTask(String id) {
        return Optional.ofNullable(tasks.get(id));
    }
    
    public void putTask(Task task) {
        tasks.put(task.getId(), task);
    }
    
    public void removeTask(String id) {
        tasks.remove(id);
    }
    
    public List<Task> getTasks() {
        return new ArrayList(tasks.values());
    }
    
}
