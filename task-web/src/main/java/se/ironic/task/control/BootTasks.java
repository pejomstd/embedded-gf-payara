package se.ironic.task.control;

import java.util.Calendar;
import java.util.Date;
import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import se.ironic.task.boundary.TaskCache;
import se.ironic.task.model.Task;

@Singleton @Startup
public class BootTasks {
    @PostConstruct
    public void initTasks() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DATE, 3);
        TaskCache.INST.putTask(Task.of("Add JAXRSConfiguration", "Add a class for JAXRS configuration", cal.getTime()));
        TaskCache.INST.putTask(Task.of("Add TaskResource", "Add a class for Task service", cal.getTime()));
        TaskCache.INST.putTask(Task.of("Add BootTasks", "Add a bean for bootstrap", cal.getTime()));
    }
}
